BEGINPROG Factorial
/* 
hello
This is 
a
Comment
*/ READ(number)              
result := 1
IF (number > -1) THEN
  WHILE (number > 0) DO 
    result := result * number
    number := number - 1  
  ENDWHILE
ELSE                      
  result := -1
ENDIF
PRINT(result)
ENDPROG
