/**
* Represents a node of the parseTree, 
* a node can either be a token or a variable
* when the parser creates a new Tree branch,
* a new Knot is created for the root of this branch
*/

public class Knot
{
    private Symbol token = null;
    private Variable variable = null;
    
    /**
    * constructor of the class Knot
    * @param token The token for the node
    */
    public Knot(Symbol token)
    {
        this.token = token;
    }
    
    /**
    * Another constructor of the class Knot
    * @param variable The variable for the node
    */
    public Knot(Variable variable)
    {
        this.variable = variable;
    }
    
    /**
    * Checks if this node is a token
    * @return true if the token is defined, otherwise false
    */
    public boolean isToken()
    {
        return (token != null);
    }
    
    /**
    * Checks if this node is a variable
    * @return true if the token is a variable otherwise false 
    */
    public boolean isVariable()
    {
        return (variable != null);
    }
    
    /**
    * Gives the value of the token argument:
    * null if this node is not a token,
    * otherwise, gives the token.
    * @return the token 
    */
    public Symbol getToken()
    {
        return token;
    }
    
    /**
    * Gives the value of the variable argument:
    * null if this node is not a variable,
    * otherwise, gives the variable.
    * @return The value of the variable argument
    */
    public Variable getVariable()
    {
        return variable;
    }
    
    /**
    * Gives the type of this node (Token, variable or terminal)
    * @return A string giving the type of this node
    */
    public String getType()
    {
        if(isToken())
            return "Token";
        else if(isVariable())
            return "Variable";
        else
            return "Not a symbol";
    }

}
