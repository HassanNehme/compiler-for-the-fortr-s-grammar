/**
* A class that represents a token; a token
* is returned from the lexer to the parser
* when scanning the input file.
* Much information is stored in the token, its LexicalUnit
* ,the line and column where it appears in the input file 
* and its value.
* This class was given with the assignment later on it was 
* slightly modified.
* @author Hassan Nehme
*/
public class Symbol{
	public static final int UNDEFINED_POSITION = -1;
	public static final Object NO_VALUE = null;
	
	private final LexicalUnit type;
	private final Object value;
	private final int line,column;
	private boolean isEpsilon = false;
    
    
    /**
    * constructor for the class symbol.
    * @param unit lexicalUnit of the token
    * @param line line in the scanned file where the lexicalUnit of the token appeared
    * @param column column in the scanned file where the lexicalUnit of the token appeared
    * @param value the value of that lexicalUnit
    */
	public Symbol(LexicalUnit unit,int line,int column,Object value){
        this.type	= unit;
		this.line	= line+1;
		this.column	= column;
		this.value	= value;
	}
	
	/**
    * Another constructor for the class symbol.
    * @param unit lexicalUnit of the token
    * @param line line in the scanned file where the lexicalUnit of the token appeared
    * @param column column in the scanned file where the lexicalUnit of the token appeared
    */
	public Symbol(LexicalUnit unit,int line,int column){
		this(unit,line,column,NO_VALUE);
	}
	
	/**
    * Another constructor for the class symbol.
    * @param unit lexicalUnit of the token
    * @param line line in the scanned file where the lexicalUnit of the token appeared
    */
	public Symbol(LexicalUnit unit,int line){
		this(unit,line,UNDEFINED_POSITION,NO_VALUE);
	}
	
	/**
    * Another constructor for the class symbol.
    * @param unit lexicalUnit of the token
    */
	public Symbol(LexicalUnit unit){
		this(unit,UNDEFINED_POSITION,UNDEFINED_POSITION,NO_VALUE);
	}
	
	/**
    * Another constructor for the class symbol.
    * @param unit lexicalUnit of the token
    * @param value the value of that lexicalUnit
    */
	public Symbol(LexicalUnit unit,Object value){
		this(unit,UNDEFINED_POSITION,UNDEFINED_POSITION,value);
	}
	
	/**
    * constructor for the class symbol.
    * @param epsilon epsilon word.
    */
	public Symbol(Terminal epsilon){
		this(null,UNDEFINED_POSITION,UNDEFINED_POSITION,epsilon);
		isEpsilon = true;
	}
	
	/**
    * checks if the current terminal represents
    * the word epsilon or not.
    * @return true if the token represents epsilon otherwise false
    */
	public boolean isEpsilon()
	{
        return isEpsilon;
	}
	
	/**
	* @return the type of the token 
	*/
	public LexicalUnit getType(){
		return this.type;
	}
	
	/**
	* @return the value of the token 
	*/
	public Object getValue(){
		return this.value;
	}
	
	/**
	* @return the line of the token 
	*/
	public int getLine(){
		return this.line;
	}
	
	/**
	* @return the column of the token 
	*/
	public int getColumn(){
		return this.column;
	}
	
	/**
	* Gives the value and the lexicalUnit of the token
	* @return a string containing the value and the lexicalUnit of the token.
	*/
	@Override
	public String toString(){
        final String value	= this.value != null? this.value.toString() : "null";
        final String type		= this.type  != null? this.type.toString()  : "null";
        return String.format("token: %-15slexical unit: %s", value, type);
    }
    
    /**
	* Gives the value of the token 
	* @return a string containing the value of the token.
	*/
    public String getStringValue(){
        final String value = this.value != null? this.value.toString() : "null";
        return String.format("%-15s", value);
    }
}
