import java.util.regex.PatternSyntaxException;

%%// Options of the scanner

%class LexicalAnalyzer //Naming the class
%unicode  //Using unicode directive
%line //Switching line counting on and accessing line number with yyline variable 
%column //Switching column counting on and accessing column number with yycolumn variable 
%function nextToken //Naming scanning method
%type Symbol //Specifiying the type of the returning values of the scanning method

%yylexthrow{
    PatternSyntaxException
%yylexthrow}


%eofval{
	return new Symbol(LexicalUnit.EOS,yyline, yycolumn);
%eofval}

//Extended Regular Expressions

AlphaUpperCase = [A-Z]
AlphaLowerCase = [a-z]
Alpha          = {AlphaUpperCase}|{AlphaLowerCase}
Numeric        = [0-9]
AlphaNumeric   = {Alpha}|{Numeric}

Number         = ([1-9][0-9]*|0)

VarIdentifier  = (({AlphaLowerCase})({AlphaLowerCase}|{Numeric})*)
ProgIdentifier = ({AlphaUpperCase}+)({AlphaLowerCase}|{Numeric}+)({AlphaNumeric}*)
CarriageReturn = "\r"
LineFeed       = "\n"
LineTerminator = ({LineFeed}{CarriageReturn}?) | ({CarriageReturn}{LineFeed}?)
Space          = (\t | " ")
Cut            = ({Space}) | ({LineTerminator})
Any            = ([^"\n""\r"])*
EndOfLine	   = {CarriageReturn}?{LineFeed}

//Declare exclusive states
%xstate YYINITIAL, COMMENT

%%//Identification of tokens

<YYINITIAL> {
	"/*"           {yybegin(COMMENT);} //go to ignore mode
	"//"{Any}    {} //ignore all

	"="        {return new Symbol(LexicalUnit.EQ,yyline, yycolumn,yytext());}
	">"        {return new Symbol(LexicalUnit.GT,yyline, yycolumn,yytext());}
	"("        {return new Symbol(LexicalUnit.LPAREN,yyline, yycolumn,yytext());}
	")"        {return new Symbol(LexicalUnit.RPAREN,yyline, yycolumn,yytext());}
	"-"        {return new Symbol(LexicalUnit.MINUS,yyline, yycolumn,yytext());}
	"+"        {return new Symbol(LexicalUnit.PLUS,yyline, yycolumn,yytext());}
	":="       {return new Symbol(LexicalUnit.ASSIGN,yyline, yycolumn,yytext());}
	"*"        {return new Symbol(LexicalUnit.TIMES,yyline, yycolumn,yytext());}
	"/"        {return new Symbol(LexicalUnit.DIVIDE,yyline, yycolumn,yytext());}
	","        {return new Symbol(LexicalUnit.COMMA,yyline, yycolumn,yytext());}
	
	"BEGINPROG"    {return new Symbol(LexicalUnit.BEGINPROG,yyline, yycolumn,yytext());}
	"ENDPROG"      {return new Symbol(LexicalUnit.ENDPROG,yyline, yycolumn,yytext());}
	"IF"           {return new Symbol(LexicalUnit.IF,yyline, yycolumn,yytext());}
	"THEN"         {return new Symbol(LexicalUnit.THEN,yyline, yycolumn,yytext());}
	"ENDIF"        {return new Symbol(LexicalUnit.ENDIF,yyline, yycolumn,yytext());}
	"ELSE"         {return new Symbol(LexicalUnit.ELSE,yyline, yycolumn,yytext());}
	"WHILE"        {return new Symbol(LexicalUnit.WHILE,yyline, yycolumn,yytext());}
	"DO"           {return new Symbol(LexicalUnit.DO,yyline, yycolumn,yytext());}
	"ENDWHILE"     {return new Symbol(LexicalUnit.ENDWHILE,yyline, yycolumn,yytext());}
	"PRINT"        {return new Symbol(LexicalUnit.PRINT,yyline, yycolumn,yytext());}
	"READ"         {return new Symbol(LexicalUnit.READ,yyline, yycolumn,yytext());}
	
	{VarIdentifier}  {return new Symbol(LexicalUnit.VARNAME,yyline, yycolumn,yytext());}
	{ProgIdentifier} {return new Symbol(LexicalUnit.PROGNAME,yyline, yycolumn,yytext());}
	{EndOfLine}      {return new Symbol(LexicalUnit.ENDLINE,yyline, yycolumn,"\\n");}
	{Number}         {return new Symbol(LexicalUnit.NUMBER,yyline, yycolumn,yytext());}
	{Cut}            {}// ignore spaces
	
	/* error fallback */
	[^]              {throw new PatternSyntaxException("Token cannot be matched!",yytext(),yyline);}	//unmatched token gives an error
}

<COMMENT> {
	[^]                 {}//ignore all character, ASCII && NON-ASCII 
	"*/"                {yybegin(YYINITIAL);} // go back to analysis
  	<<EOF>>             {throw new PatternSyntaxException("Unclosed comment!",yytext(),yyline);} //End of file reached before closing comment
}

