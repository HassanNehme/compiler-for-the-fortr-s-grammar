/** 
* Enumerates all the possible variables of the FORT-S grammar.
*/
public enum Variable{
    Program,
    Code,
    Instruction,
    Assign,
    ExprArith,
    Primary,
    ExprTail,
    PrimaryTail,
    Expr,
    Add,
    Mult,
    If,
    IfTail,
    Cond,
    Comp,
    While,
    Print,
    Read
}
