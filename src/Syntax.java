import java.util.*;
/**
* A class containing all necessary methods
* to write a LLVM code.
*/
public class Syntax
{
    private StringBuilder code;
    private Set<String> variables;
    private boolean printDeclared = false;
    private boolean readDeclared = false;
    private int register_id;
    private int if_id;
    private int while_id;
    
    /**
    * Constructor for the class Syntax
    */
    public Syntax()
    {
        code = new StringBuilder();
        variables = new HashSet<String>();
        if_id = 1;
        while_id = 1;
        start_code();
    }
    
    /**
    * Adds the very first lines of LLVM code to the 
    * StringBuilder "code": defining main and starting
    * the entry scope.
    */
    public void start_code() 
    {
        code.append("define i32 @execute()\n{\n");
        code.append("entry:\n");
        allocateRegister();
    }
    /**
    * @return register_id 
    */
    public int getRegister_id() 
    {
        return register_id;
    }
    
    /**
    * @param register_id: to replace the current register_id argument.
    */
    public void setRegister_id(int register_id)
    {
        this.register_id = register_id;
    }
    
    
    /**
    * Adds to "code" the LLVM code to load a variable.
    * @param variable: the variable to load
    * @throws Exception: an exception declaring that the variable to load was not assigned.
    * @return the current register_id;
    */
    public int LoadVariable(String variable) throws Exception
    {
        if(variables.contains(variable))
        {
            code.append("\t%" + register_id + " = load i32 , i32* %" + variable + "\n");
            int current_register_id = register_id;
            register_id++;
            return current_register_id;
        }
        else
            throw new Exception("Undeclared variable");
    }
    
    /**
    * Adds to "code" the LLVM code to load a register.
    * @param register: the register to load
    * @return the current register_id;
    */
    public int LoadRegister(int register)
    {
        code.append("\t%" + register_id + " = load i32 , i32* %" + register + "\n");
        int current_register_id = register_id;
        register_id++;
        return current_register_id;
    }
    
    /**
    * Adds to "code" the LLVM code to allocate memory for a variable.
    * @param variable: the variable to allocate memory for.
    */
    public void allocateVariable(String variable)
    {
        code.append("\t%" + variable + " = alloca i32\n");
        variables.add(variable);
    }
    
    /**
    * Adds to "code" the LLVM code to allocate memory for a new register.
    */
     public void allocateRegister()
    {
        code.append("\t%" + register_id + " = alloca i32\n");
        register_id++;
    }
    
    /**
    * Adds to "code" the LLVM code to assign a value to a variable.
    * @param variable: The variable to be assigned a new value.
    * @param value: The value to assign to the variable.
    */
    public void assign(String variable, int value)
    {
        if(!variables.contains(variable))
            allocateVariable(variable);
        code.append("\tstore i32 %" + value + ",i32* %" + variable + "\n");
    }

    /**
    * Allocates memory for a new register,
    * Store a value in the current register,
    * then load that register.
    * @param value: The value to assign to the new register.
    * @return the register to contain the value.
    */
    public int allocateAndStore(String value) 
    {
        allocateRegister();
        StoreValueInRegister(value, register_id-1);
        LoadRegister(register_id-1);
        return register_id-1;
    }

    /**
    * Stores a value in a given register
    * @param value: The value to store in the register
    * @param register: The register to contain the value.
    */
    public void StoreValueInRegister(String value, int register)
    {
        code.append("\tstore i32 " + value + ",i32* %" + register + "\n");
    }
    
    /**
    * Stores a register value in a given variable
    * @param register: The register containing the value.
    * @param variable: The variable to contain the value.
    */
    public void StoreRegisterValueInVariable(int register, String variable)
    {
        code.append("\tstore i32 %" + register  + ",i32* %" + variable + "\n");
    }
    
    /**
    * Stores a value in a given variable
    * @param value: The value to store in the variable
    * @param variable: The variable to the value.
    */
    public void StoreValueInVariable(int value, String variable)
    {
        code.append("\tstore i32 %" + value  + ",i32* %" + variable + "\n");
    }
    
    /**
    * Adds to "code" the LLVM code to compute a mathematical expression.
    * @param op: The operation to make.
    * @param a: The left hand side of the operation.
    * @param b: The right hand side of the operation.
    * @return the current register.
    */
    public int operation(String op, String a, String b)
    {
        code.append("\t%" + register_id + " = " + op + " i32 " + a + "," + b + "\n");
        int current_register_id = register_id;
        register_id++;
        return current_register_id;
    }
    
    /**
    * Adds to "code" the LLVM code to compute a mathematical expression.
    * @param op: The operation to make.
    * @param first_register: The register containing the left hand side of the operation.
    * @param second_register: The register containing the right hand side of the operation.
    * @return the current register.
    */
    public int RegistersOperation(String op, int first_register, int second_register)
    {
        code.append("\t%" + register_id + " = " + op + " i32 %" + first_register + ", %" + second_register + "\n");
        int current_register_id = register_id;
        register_id++;
        return current_register_id;
    }
    
    /**
    * Adds to "code" the LLVM code to compute a mathematical expression.
    * @param op: The operation to make.
    * @param number: The left hand side of the operation.
    * @param second_register: The register containing the right hand side of the operation.
    * @return the current register.
    */
    public int RegisterIntOperation(String op, int number, int second_register)
    {
        code.append("\t%" + register_id + " = " + op + " i32 " + number + ", %" + second_register + "\n");
        int current_register_id = register_id;
        register_id++;
        return current_register_id;
    }
    /**
    * Adds to "code" the LLVM code to print a variable.
    * @param variable: The variable to print.
    * @throws Exception: an exception declaring that the variable to print was not assigned.
    */
    public void Print(String variable) throws Exception
    {
        if(!printDeclared)
        {
            code.insert(0 ,declarePrint()+"\n");
            printDeclared = true;
        }
        if(variables.contains(variable))
        {
            code.append("\t%" + register_id + " = load i32 , i32* %" + variable + "\n");
            code.append("\tcall void @println(i32 %" + register_id + ")\n");
            register_id++;
        }
        else
        {
            throw new Exception("Undeclared variable");
        }
    }
    
    /**
    * Adds to "code" the LLVM code to read a variable.
    * @param variable: The variable to read.
    */
    public void Read(String variable)
    {
        if(!readDeclared)
        {
            code.insert(0 ,declareRead()+"\n");
            readDeclared = true;
        }
        code.append("\t%" + register_id + " = call i32 @readInt()\n");
        if(!variables.contains(variable))
            allocateVariable(variable);
        StoreRegisterValueInVariable(register_id, variable);
        register_id++;
    }

    /**
    * Adds to "code" the LLVM code to compute a boolean expression.
    * @param op: The comparison operator.
    * @param first_register: The register containing the left hand side of the operation.
    * @param second_register: The register containing the right hand side of the operation.
    * @return the current register.
    */
    public int RegistersBooleanOperation(String op, int first_register, int second_register)
    {
        code.append("\t%" + register_id + " = icmp " + op + " i32 %" + first_register + ", %" + second_register + "\n");
        int current_register_id = register_id;
        register_id++;
        return current_register_id;
    }
    
     /**
    * Adds to "code" the LLVM code to start an if statement.
    * @return the if_id of the if statement.
    */
    public int IfLabel()
    {
        code.append("\tbr label %if" + if_id + "\n");
        code.append("\tif" + if_id + ":\n");
        int current_if_id = if_id;
        if_id++;
        return current_if_id;
    }
    
    
    /**
    * Adds to "code" the LLVM code to jump to an if scope.
    * @param if_id: the id of the if statement.
    * @param register_id: the register containing the evaluation of the condition.
    * @param label: a label for the scope to jump to when the if condition is not verified, this could be the else score or what comes after the if statement when there is no else.
    */
    public void If(int if_id, int register_id, String label) 
    {
        code.append("\tbr i1 %" + register_id + ", label %innerIf" + if_id + " ,label %" + label + if_id + " \n");
        code.append("\tinnerIf" + if_id + ": \n");
    }
    
    /**
    * Adds to "code" the LLVM code to start to an else scope.
    * @param if_id: the id of the if statement.
    */
    public void ElseLabel(int if_id)
    {
        code.append("\tbr label %endif" + if_id + "\n");
        code.append("\telse" + if_id + ":\n");
    }
    
    /**
    * Adds to "code" the LLVM code to start an "after_if" scope and to jump instantly to that scope.
    * @param if_id: the if_id of the if statement.
    */
     public void Endif(int if_id) 
    {
        code.append("\tbr label %endif" + if_id + "\n");
        code.append("\tendif" + if_id + ":\n");
    }
    
    /**
    * Adds to "code" the LLVM code to start a while loop
    * @return the while_id of that loop
    */
    public int Whilelabel()
    {
        code.append("\tbr label %while" + this.while_id + "\n");
        code.append("\twhile" + this.while_id + ":\n");
        int current_while_id = while_id;
        while_id++;
        return  current_while_id;
    }
    
    /**
    * Adds to "code" the LLVM code to jump to while scopes.
    * @param while_id: the while_id of the while loop.
    * @param register_id: the register containing the evaluation of the condition. 
    * @return the register containing the value to be evaluated before choosing to exit or enter the loop.
    */
    public int While(int while_id, int register_id)
    {
        code.append("\tbr i1 %" + register_id + ", label %innerWhile" + while_id + ", label %afterWhile" + while_id + " \n");
        code.append("\tinnerWhile" + while_id + ": \n");
        return register_id;
    }

    /**
    * Adds to "code" the LLVM code to end the while loop.
    * @param while_id: the while_id of the while loop.
    */
    public void JumpWhile(int while_id)
    {
        code.append("\tbr label %while" + while_id +"\n");
    }

    /**
    * Adds to "code" the LLVM code to exit the while loop.
    * @param while_id: the while_id of the while loop.
    */
    public void AfterWhile(int while_id)
    {
        code.append("\tafterWhile" + while_id + ": \n");
    }
    
    /**
    * Declares the read method in LLVM
    * @return the read method in String format.
    */
    private String declareRead() {
        String readFunction= "; Declaring the Read method\n"+
                "@.strR = private unnamed_addr constant [3 x i8] c\"%d\\00\", align 1\n\n"+
                "; Function Attrs: nounwind uwtable\n" +
                "define i32 @readInt() #0 {\n" +
                "  %x = alloca i32, align 4\n" +
                "  %1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.strR, i32 0, i32 0), i32* %x)\n" +
                "  %2 = load i32, i32* %x, align 4\n" +
                "  ret i32 %2\n}\n\n" +
                "declare i32 @__isoc99_scanf(i8*, ...) #1";
        return readFunction;
    }
    
    /**
    * Declares the print method in LLVM
    * @return the print method in String format.
    */
    public String declarePrint() {
        String printFunction="; Declaring the Print method\n" +
                "@.strP = private unnamed_addr constant [4 x i8] c\"%d\\0A\\00\", align 1\n\n"+
                "; Function Attrs: nounwind uwtable\n" +
                "define void @println(i32 %x) #0 {\n" +
                "  %1 = alloca i32, align 4\n" +
                "  store i32 %x, i32* %1, align 4\n" +
                "  %2 = load i32, i32* %1, align 4\n" +
                "  %3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.strP, i32 0, i32 0), i32 %2)\n" +
                "  ret void\n}\n\n" +
                "declare i32 @printf(i8*, ...) #1\n";
        return printFunction;
    }
    
    /**
    * Takes a mathematical comparison operator, 
    * and gives the corresponding LLVM syntax to that
    * operator.
    * @param comp_operator the comparison operator.
    - @return the operator in LLVM syntax.
    */
    public String getCompOperator(String comp_operator) 
    {
        switch (comp_operator)
        {
            case "=":
                return "eq";
            case ">":
                return "sgt";
        }
        return null;
    }
     
     /**
     * Adds to "code" the last lines of LLVM code needed. 
     * @return the whole code in String format.
     */
     public String toString()
     {
        code.append("\tret i32 0\n");
        code.append("}\n\n");
        code.append("define i32 @main() \n{\n");
        code.append("entry:\n"); 
        code.append("\t%0 = call i32 @execute()\n");
        code.append("\tret i32 0\n}");
        return code.toString();
    }

}
