import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.*;
/** Main class  
*  @author Hassan Nehme
*  @version 1.8
*/
public class Main
{       
    /**
    * Reads a FORT-S file given in the last argument and writes
    * on the standard output, or in a file the LLVM code that corresponds to 
    * provided input after finishing the lexical analysis phase and
    * the parsing phase. 
    * @param args INPUT_FILE, "-o" OUTPUT_FILE if the llvm code must be put in a file, "-exec" INPUT_FILE if the input file must be interpreted.
    * @throws IOException If an input or output exception occured.
    * @throws FileNotFoundException If the file provided as input is not found.
    * @throws SecurityException If there is a security violation.
    * @throws ParseException If there is a parsing error 
    */
    public static void main(String args[]) throws IOException, FileNotFoundException, SecurityException, ParseException
    {   
        boolean print_in_file = false;
        boolean interpret = false;
        int argc = args.length;
        if( argc == 0)
        {  
           System.out.println("java -jar part3.jar [INPUT_FILE]  : For stdout || java -jar part3.jar [FILE_INPUT] -o [FILE_OUTPUT] : for a specific output file || java -jar part3.jar -exec [INPUT_FILE]  : For interpreting, to be executed with lli out.bc");
           System.exit(1);
        }
        
        FileReader reader = null;
        FileWriter writer = null;
        
        if(argc > 1)
        {
            if(args[1].toString().equals("-o"))
            {
                try{
                    reader = new FileReader(args[argc-3]);
                } catch(FileNotFoundException e){
                    System.out.println(String.format("File not found: %s", args[argc-3]));
                    System.exit(1);
                }
                writer = new FileWriter(args[argc-1]);
                print_in_file = true;
            }
            else if(args[0].toString().equals("-exec"))
            {
                try{
                    reader = new FileReader(args[argc-1]);
                } catch(FileNotFoundException e){
                    System.out.println(String.format("File not found: %s", args[argc-1]));
                    System.exit(1);
                }
                interpret = true;
            }
        }
        else
        {
            try{
                reader = new FileReader(args[argc-1]);
            } catch(FileNotFoundException e){
                System.out.println(String.format("File not found: %s", args[argc-1]));
                System.exit(1);
            }
        }
        
        Parser parser = new Parser(reader);
        ParseTree tree = null;
    
        try{
            tree = parser.start();
        }catch(ParseException e)
        {
               System.out.println(e.getMessage());
               System.exit(1);
        }


        Generator generator = new Generator(tree);
        try
        {
            generator.Generate();
        }
        catch(ParseException e)
        {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        if(print_in_file)
        {
            writer.write(generator.getLLVMcode());
            writer.close();
        }
        else if(interpret)
        {   
            String output_file = "out";
            writer = new FileWriter(output_file+".ll");
            writer.write(generator.getLLVMcode());
            writer.close();
            String command = "llvm-as " + output_file + ".ll -o " + output_file + ".bc";
            Process process = Runtime.getRuntime().exec(command);
        }
        else
            System.out.println(generator.getLLVMcode());
        
    }
}
