/**
* An LLVM code generator from a ParseTree
*/
public class Generator 
{
    ParseTree parseTree;
    Syntax Codellvm;
    
    /**
    * Constructor for the class Generator
    */
    public Generator(ParseTree parseTree) 
    {
        this.parseTree = parseTree;
        this.Codellvm = new Syntax();
    }
    
    /**
    * Initializes the LLVM code generation, 
    * by taking the parseTree provided by the 
    * parser and finding the tree with label <Code>.
    * @throws ParseException: any exception related to parsing.
    */
    public void Generate() throws ParseException
    {
        
        for (int i = 0; i < parseTree.getChildren().size(); i++)
        {
            Knot node = parseTree.getChildren().get(i).getKnot();
            if (node.getVariable() != null) 
            {
                if (node.getVariable() == Variable.Code)
                {
                    Code(this.parseTree.getChildren().get(i));
                    break;
                }
            }

        } 

    }
    
    /**
    * If the Tree has children calls the Instruction method.
    * @param codeTree: the tree with a root labeled by <Code>.
    * @throws ParseException: any exception related to parsing.
    */
    private void Code(ParseTree codeTree) throws ParseException
    {
        if(codeTree.getChildren().size() > 1 )
        {
            Instruction(codeTree.getChildren().get(0));
            Code(codeTree.getChildren().get(2));
        }
    }
    
    /**
    * Looks at the first children of the tree 
    * and based on that children calls the corresponding 
    * method to generate the LLVM code.
    * @param InstructionTree: the tree with a root labeled by <Instruction>.
    * @throws ParseException: any exception related to parsing.
    */
    private void Instruction(ParseTree InstructionTree) throws ParseException
    {
        switch (InstructionTree.getChildren().get(0).getKnot().getVariable())
        {
        
        case Assign:
            Assign(InstructionTree.getChildren().get(0)); break;
        case If:
            If(InstructionTree.getChildren().get(0)); break;
        case While:
            While(InstructionTree.getChildren().get(0)); break;
        case Print:
            Print(InstructionTree.getChildren().get(0)); break;
        case Read:
            Read(InstructionTree.getChildren().get(0)); break;
        }
    }

     /**
    * The children is labeled by <Assign>, 
    * calls the assign method in Syntax class 
    * to generate the code.
    * @param assignTree: the tree with a root labeled by <Assign>
    * @throws ParseException: any exception related to parsing.
    */
    private void Assign(ParseTree assignTree) throws ParseException
    {
        String variable = assignTree.getChildren().get(0).getKnot().getToken().getValue().toString();
        int expression = ExprArith(assignTree.getChildren().get(2));
        this.Codellvm.assign(variable, expression);
    }
    
    /**
    * Used when the children is labeled by <ExprArith>.
    * @param exprArithTree: the tree with a root labeled by <ExprArith>.
    * @throws ParseException: any exception related to parsing.
    * @return a register id with a computed expression.
    */
    private int ExprArith(ParseTree exprArithTree) throws ParseException
    {
        int left_side = Primary(exprArithTree.getChildren().get(0));
        return ExprTail(left_side, exprArithTree.getChildren().get(1));
    }
    
    /**
    * Used when the children is labeled by <Primary>.
    * @param PrimaryTree: the tree with a root labeled by <Primary>.
    * @throws ParseException: any exception related to parsing.
    * @return a register id with a  computed expression.
    */
    private int Primary(ParseTree PrimaryTree) throws ParseException
    {
        int left_side = Expr(PrimaryTree.getChildren().get(0));
        return PrimaryTail(left_side, PrimaryTree.getChildren().get(1));
    }
    
    /**
    * Used when the children is labeled by <PrimaryTail>.
    * @param left_side: the computed left children of the parent tree.
    * @param PrimaryTailTree: the tree with a root labeled by <PrimaryTail>.
    * @throws ParseException: any exception related to parsing.
    * @return a register id with a computed expression.
    */
    private int PrimaryTail(int left_side ,ParseTree PrimaryTailTree) throws ParseException
    {
    if(PrimaryTailTree.getChildren().size() > 1)
    {
            switch(PrimaryTailTree.getChildren().get(0).getChildren().get(0).getKnot().getToken().getType())
            {
                case TIMES:
                    int left_multiplication = Codellvm.RegistersOperation("mul", left_side, Expr(PrimaryTailTree.getChildren().get(1)));
                    return PrimaryTail(left_multiplication, PrimaryTailTree.getChildren().get(2));
                case DIVIDE:
                    int left_division = this.Codellvm.RegistersOperation("sdiv", left_side, Expr(PrimaryTailTree.getChildren().get(1))); 
                    return PrimaryTail(left_division, PrimaryTailTree.getChildren().get(2));
            }
    }
    return left_side;
    }
    
    /**
    * Used when the children is labeled by <ExprTail>.
    * @param left_side: the computed left children of the parent tree.
    * @param ExprTailTree: the tree with a root labeled by <ExprTail>.
    * @throws ParseException: any exception related to parsing.
    * @return a register id with a computed expression.
    */
    private int ExprTail(int left_side ,ParseTree ExprTailTree) throws ParseException
    {
        if(ExprTailTree.getChildren().size() > 1 )
        {
            switch(ExprTailTree.getChildren().get(0).getChildren().get(0).getKnot().getToken().getType())
            {
                case PLUS:
                    int left_sum = Codellvm.RegistersOperation("add", left_side, Primary(ExprTailTree.getChildren().get(1))); 
                    return ExprTail(left_sum, ExprTailTree.getChildren().get(2));
                case MINUS:
                    int left_sub = this.Codellvm.RegistersOperation("sub", left_side, Primary(ExprTailTree.getChildren().get(1)));
                    return ExprTail(left_sub, ExprTailTree.getChildren().get(2));
            }
        }
        return left_side;
    }
    
    /**
    * Used when the children is labeled by <ExprTree>.
    * @param ExprTree: the tree with a root labeled by <ExprTree>.
    * @throws ParseException: any exception related to parsing.
    * @return a register id containing a computed expression.
    */
    private int Expr(ParseTree ExprTree) throws ParseException
    {
        Integer result_register = null;
        Symbol ExprToken = ExprTree.getChildren().get(0).getKnot().getToken();
        switch (ExprToken.getType())
        {
            case VARNAME:
                try
                {
                    result_register  = Codellvm.LoadVariable(ExprToken.getValue().toString()); break;
                }catch(Exception e){
                    throw new ParseException(e.getMessage(), ExprToken);
                }
            case NUMBER:
                result_register = Codellvm.allocateAndStore(ExprToken.getValue().toString()); break;
            case MINUS:
                result_register = Codellvm.RegisterIntOperation("sub nsw", 0 , Expr(ExprTree.getChildren().get(1))); break;
            case LPAREN:
                result_register = ExprArith(ExprTree.getChildren().get(1)); break;
        }
        
        return result_register;
    }
    /**
    * Used when the children is labeled by <Print>.
    * @param PrintTree: the tree with a root labeled by <Print>.
    * @throws ParseException: any exception related to parsing.
    */
    private void Print(ParseTree PrintTree) throws ParseException
    {
        Symbol PrintToken = PrintTree.getChildren().get(2).getKnot().getToken();
        String variable = PrintTree.getChildren().get(2).getKnot().getToken().getValue().toString();
        try
        {
            Codellvm.Print(variable);
        }catch(Exception e)
        {
            throw new ParseException(e.getMessage(), PrintToken); 
        }

    }
    
    /**
    * Used when the children is labeled by <Read>.
    * @param ReadTree: the tree with a root labeled by <Read>.
    * @throws ParseException: any exception related to parsing.
    */
    private void Read(ParseTree ReadTree) 
    {
        String variable = ReadTree.getChildren().get(2).getKnot().getToken().getValue().toString();
        Codellvm.Read(variable);
    }
    
    /**
    * Used when the children is labeled by <Comp>.
    * @param compTree: the tree with a root labeled by <Comp>.
    * @param left_hand_side: a register id containing the computed left_hand_side.
    * @param right_hand_side: a register id containing the computed right_hand_side.
    * @throws ParseException: any exception related to parsing.
    */
    private int Comp(ParseTree compTree, int left_hand_side, int right_hand_side) throws ParseException
    {
        String operation = compTree.getChildren().get(0).getKnot().getToken().getValue().toString();
        String llvm_operation = Codellvm.getCompOperator(operation);
        return Codellvm.RegistersBooleanOperation(llvm_operation, left_hand_side, right_hand_side);
    }
    
    /**
    * Used when the children is labeled by <Cond>.
    * @param condTree: the tree with a root labeled by <Cond>.
    */
    private int Cond(ParseTree condTree) throws ParseException
    {
        int left_hand_side = ExprArith(condTree.getChildren().get(0));
        int right_hand_side = ExprArith(condTree.getChildren().get(2));
        return Comp(condTree.getChildren().get(1), left_hand_side, right_hand_side);
    }
    
     /**
    * Used when the children is labeled by <If>.
    * @param ifTree: the tree with a root labeled by <If>.
    * @throws ParseException: any exception related to parsing.
    */
    private void If(ParseTree ifTree) throws ParseException
    {
        int if_id = Codellvm.IfLabel();
        int cond_register = Cond(ifTree.getChildren().get(2));
        if(ifTree.getChildren().get(7).getChildren().size() > 1)
        {
            Codellvm.If(if_id, cond_register, "else");
            Code(ifTree.getChildren().get(6));
            IfTail(ifTree.getChildren().get(7), if_id);
        }
        else
        {
            Codellvm.If(if_id, cond_register, "endif");
            Code(ifTree.getChildren().get(6));
            Codellvm.Endif(if_id);
        }
    }
    
     /**
    * Used when the children is labeled by <IfTail>.
    * @param ifTailTree: the tree with a root labeled by <IfTail>.
    * @param if_id: the id of the if statement.
    * @throws ParseException: any exception related to parsing.
    */
     private void IfTail(ParseTree ifTailTree, int if_id) throws ParseException
    {
        Codellvm.ElseLabel(if_id);
        Code(ifTailTree.getChildren().get(2));
        Codellvm.Endif(if_id);
    }
    
    /**
    * Used when the children is labeled by <While>.
    * @param WhileTree: the tree with a root labeled by <While>.
    * @throws ParseException: any exception related to parsing.
    */
    private void While(ParseTree WhileTree) throws ParseException
    {
        int while_id = Codellvm.Whilelabel();
        int cond_register = Cond(WhileTree.getChildren().get(2));
        int end_register = Codellvm.While(while_id, cond_register);
        Code(WhileTree.getChildren().get(6));
        Codellvm.JumpWhile(while_id);
        Codellvm.AfterWhile(while_id);
    }
    
    /**
    * Get the LLVM in a String format.
    * @return  the whole LLVM code.
    */
    public String getLLVMcode()
    {
        return Codellvm.toString();
    }

}
