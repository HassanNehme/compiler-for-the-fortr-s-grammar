import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

/** 
 * A ParseException is an exception that is raised when 
 * there is an error related to parsing.
 * @author HASSAN NEHME
 */
public class ParseException extends Exception {
    /**
    * When the token cannot be matched the line 
    * where the token appeared in the input file 
    * is printed-out.
    * @param token The token provided by the lexer.
    */
    public ParseException(Symbol token)
    {
        super(String.format("Syntax error at line %d", token.getLine()));
    }
    
    /**
    * Any other type of exception related to parsing
    * will be handled by this method, for instance
    * a variable that needs to be printed but was not 
    * declared.
    * @param ErrorMessage The parsing error message to print on the stdout
    * @param token The token provided by the lexer.
    */
     public ParseException(String ErrorMessage, Symbol token)
    {
        super(String.format(ErrorMessage + " at line %d", token.getLine()));
    }
}
