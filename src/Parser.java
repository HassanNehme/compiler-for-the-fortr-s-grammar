import java.util.List;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;

/**
 * A recursive descent LL(1) parser for FORT-S language.
 * For each production rule a method was implemented
 * The method tries to match the token given by the lexer or 
 * pass it until a terminal is reached. 
 * @author HASSAN NEHME
 */

public class Parser 
{
    private LexicalAnalyzer Lexer;
    private Symbol token; 
    private ArrayList<Integer> Derivation;
    
    public Parser(FileReader source) throws IOException
    {
        Lexer = new LexicalAnalyzer(source);
        Derivation = new ArrayList<Integer>();
        token = Lexer.nextToken();
    }
    /**
    * Tries to match the token given by the lexer.
    * @throws IOException If an input or output exception occured.
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree match(Terminal terminal) throws IOException, ParseException
    {   
//         System.out.println(String.format("token type: %s and terminal: %s", token.getType(), terminal));
        if(!token.getType().toString().equals(terminal.toString()))
        {   
            throw new ParseException(token);
        }
        else 
        {
            Symbol currentToken = token;
            token = Lexer.nextToken();
            return new ParseTree(currentToken);
        }
    }
    
    /**
    * Initializes the parser, starts by matching the tokens BEGINPROG 
    * , PROGNAME, ENDLINE then calls the code method and finally tries 
    * to match the last token ENDPROG
    * @throws IOException If an input or output exception occured.
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    public ParseTree start() throws IOException, ParseException
    {   
        Derivation.add(1);
        return new ParseTree(Variable.Program, Arrays.asList(match(Terminal.BEGINPROG), match(Terminal.PROGNAME) ,match(Terminal.ENDLINE), Code(), match(Terminal.ENDPROG)));
    }
    
     /**
    * Used when the second/third rules of the grammar need to be matched.
    * @throws IOException If an input or output exception occured.
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Code() throws IOException, ParseException
    {
        switch(token.getType()) 
        {
            case VARNAME:
            case IF:
            case WHILE:
            case PRINT:
            case READ:
                Derivation.add(2);
                return new ParseTree(Variable.Code, Arrays.asList(Instruction(), match(Terminal.ENDLINE), Code() ));
            case ELSE:
            case ENDIF:
            case ENDWHILE:
            case ENDPROG:
                Derivation.add(3);
                return new ParseTree(Variable.Code, Arrays.asList(new ParseTree(Terminal.EPSILON)));
            default:
                throw new ParseException(token);
        }
    }
    
    /**
    * Used when the fourth/fifth/sixth/seventh/eighth rule of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured.
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Instruction() throws IOException, ParseException
    {
        switch(token.getType()) 
        {
            case VARNAME:
                Derivation.add(4);
                return new ParseTree(Variable.Instruction, Arrays.asList(Assign()));
            case IF:
                Derivation.add(5);
                return new ParseTree(Variable.Instruction, Arrays.asList(If()));
            case WHILE:
                Derivation.add(6);
                return new ParseTree(Variable.Instruction, Arrays.asList(While()));
            case PRINT:
                Derivation.add(7);
                return new ParseTree(Variable.Instruction, Arrays.asList(Print()));
            case READ:
                Derivation.add(8);
                return new ParseTree(Variable.Instruction, Arrays.asList(Read())); 
            default:
                throw new ParseException(token);
        }
    }
    
    /**
    * Used when the nineth rule of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Assign() throws IOException, ParseException
    {   
        Derivation.add(9);
        return new ParseTree(Variable.Assign, Arrays.asList(match(Terminal.VARNAME), match(Terminal.ASSIGN), ExprArith())); 
    }
    
     /**
    * Used when the thenth rule of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree. 
    */
    private ParseTree ExprArith() throws IOException, ParseException
    {   
        Derivation.add(10);
        return new ParseTree(Variable.ExprArith, Arrays.asList(Primary(), ExprTail()));  
    }
    
     /**
    * Used when the eleventh rule of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Primary() throws IOException, ParseException
    {   
        Derivation.add(11);
        return new ParseTree(Variable.Primary, Arrays.asList(Expr(), PrimaryTail()));    
    }
    
     /**
    * Used when the twelveth/thirteenth rule of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree ExprTail() throws IOException, ParseException
    {
         switch(token.getType()) 
         {
            case PLUS:
            case MINUS:
                Derivation.add(12);
                return new ParseTree(Variable.ExprTail, Arrays.asList(Add(), Primary(), ExprTail()));
            case EQ:
            case GT:
            case RPAREN:
            case ENDLINE:
                Derivation.add(13);
                return new ParseTree(Variable.ExprTail, Arrays.asList(new ParseTree(Terminal.EPSILON)));
            default:
                throw new ParseException(token);
        }
    }
    
     /**
    * Used when the fourteenth/fifteenth rule of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree PrimaryTail() throws IOException, ParseException
    {
        switch(token.getType()) 
         {
            case TIMES:
            case DIVIDE:
                Derivation.add(14);
                return new ParseTree(Variable.PrimaryTail, Arrays.asList(Mult(), Expr(), PrimaryTail()));
            case EQ:
            case GT:
            case RPAREN:
            case ENDLINE:
            case PLUS:
            case MINUS:
                Derivation.add(15);
                return new ParseTree(Variable.PrimaryTail, Arrays.asList(new ParseTree(Terminal.EPSILON)));
            default:
                throw new ParseException(token);
        }
    }
    
     /**
    * Used when the sixteenth/seventeenth/eighteenth/ninteeth rule of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Expr() throws IOException, ParseException
    {
        switch(token.getType()) 
         {
            case VARNAME:
                Derivation.add(16);
                return new ParseTree(Variable.Expr, Arrays.asList(match(Terminal.VARNAME)));
            case MINUS:
                Derivation.add(17);
                return new ParseTree(Variable.Expr, Arrays.asList(match(Terminal.MINUS), Expr()));
            case NUMBER:
                Derivation.add(18);
                return new ParseTree(Variable.Expr, Arrays.asList(match(Terminal.NUMBER)));
            case LPAREN:
                Derivation.add(19);
                return new ParseTree(Variable.Expr, Arrays.asList(match(Terminal.LPAREN), ExprArith(), match(Terminal.RPAREN)));
            default:
                throw new ParseException(token);
        }
    }
    
    /**
    * Used when the rule number twenty/twenty-one of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Add() throws IOException, ParseException
    {
        switch(token.getType()) 
        {
            case PLUS:
                Derivation.add(20);
                return new ParseTree(Variable.Add, Arrays.asList(match(Terminal.PLUS)));
            case MINUS:
                Derivation.add(21);
                return new ParseTree(Variable.Add, Arrays.asList(match(Terminal.MINUS)));
            default:
                throw new ParseException(token);
        }
    }
    
    /**
    * Used when the rule number twenty-two/twenty-three of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Mult() throws IOException, ParseException
    {
        switch(token.getType()) 
        {
            case TIMES:
                Derivation.add(22);
                return new ParseTree(Variable.Mult, Arrays.asList(match(Terminal.TIMES)));
            case DIVIDE:
                Derivation.add(23);
                return new ParseTree(Variable.Mult, Arrays.asList(match(Terminal.DIVIDE)));
            default:
                throw new ParseException(token);
        }
    }
    
    /**
    * Used when the rule number twenty-four of the grammar needs to be matched.    
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree 
    */
    private ParseTree If() throws IOException, ParseException
    {
        Derivation.add(24);
       return new ParseTree(Variable.If, Arrays.asList(match(Terminal.IF), match(Terminal.LPAREN), Cond(), match(Terminal.RPAREN), match(Terminal.THEN), match(Terminal.ENDLINE), Code(), IfTail()));
    }
    
    /**
    * Used when the rule number twenty-five/twenty-six of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree IfTail() throws IOException, ParseException
    {
        switch(token.getType()) 
        {
            case ELSE:
                Derivation.add(25);
                return new ParseTree(Variable.IfTail, Arrays.asList(match(Terminal.ELSE), match(Terminal.ENDLINE), Code(), match(Terminal.ENDIF)));
            case ENDIF: 
                Derivation.add(26);
                return new ParseTree(Variable.IfTail, Arrays.asList(match(Terminal.ENDIF)));
            default:
                throw new ParseException(token);
        }
    }
    
    /**
    * Used when the rule number twenty-seven of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Cond() throws IOException, ParseException
    {
        Derivation.add(27);
        return new ParseTree(Variable.Cond, Arrays.asList(ExprArith(), Comp(), ExprArith()));
    }
    
     /**
    * Used when the rule number twenty-eight/twenty-nine of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Comp() throws IOException, ParseException
    {   
        switch(token.getType()) 
        {
            case EQ:
                Derivation.add(28);
                return new ParseTree(Variable.Comp, Arrays.asList(match(Terminal.EQ)));
            case GT:
                Derivation.add(29);
                return new ParseTree(Variable.Comp, Arrays.asList(match(Terminal.GT)));
            default:
                throw new ParseException(token);
        }
    }
    
    /**
    * Used when the rule number thirty of the grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree While() throws IOException, ParseException
    {
        Derivation.add(30);
        return new ParseTree(Variable.While, Arrays.asList(match(Terminal.WHILE), match(Terminal.LPAREN), Cond() ,match(Terminal.RPAREN), match(Terminal.DO), match(Terminal.ENDLINE), Code(), match(Terminal.ENDWHILE)));
    }
    
    /**
    * Used when the rule number thirty-one of grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Print() throws IOException, ParseException
    {
        Derivation.add(31);
        return new ParseTree(Variable.Print, Arrays.asList(match(Terminal.PRINT), match(Terminal.LPAREN), match(Terminal.VARNAME), match(Terminal.RPAREN)));
    }
    
    /**
    * Used when the rule number thirty-two of grammar needs to be matched.
    * @throws IOException If an input or output exception occured. 
    * @throws ParseException If the token provided by the lexer cannot be matched.
    * @return new ParseTree.
    */
    private ParseTree Read() throws IOException, ParseException
    {
        Derivation.add(32);
        return new ParseTree(Variable.Read, Arrays.asList(match(Terminal.READ), match(Terminal.LPAREN), match(Terminal.VARNAME), match(Terminal.RPAREN)));
    }
    
    public ArrayList<Integer> getDerivation()
    {
        return Derivation;
    }
}
