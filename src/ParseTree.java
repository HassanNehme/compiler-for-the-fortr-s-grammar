import java.util.List;
import java.util.ArrayList;

/**
 * A skeleton class to represent parse trees. The arity is not fixed: a node can
 * have 0, 1 or more children. Trees are represented in the following way: Tree
 * :== Symbol * List<Tree> In other words, trees are defined recursively: A tree
 * is a root (with a label of type Symbol) and a list of trees children. Thus, a
 * leave is simply a tree with no children (its list of children is empty). This
 * class can also be seen as representing the Node of a tree, in which case a
 * tree is simply represented as its root.
 * Originally, this class was written by Sarah Winter and Léo Exibard later on it was slightly modified.
 * @author Hassan Nehme
 */

public class ParseTree {
    private Knot label = null; // The label of the root of the tree
    private List<ParseTree> children; // Its children, which are trees themselves

    /**
     * Creates a singleton tree with only a root labeled by lbl.
     * 
     * @param lbl The label of the root
     */
    public ParseTree(Symbol lbl) 
    {
        label = new Knot(lbl);
        this.children = new ArrayList<ParseTree>(); // This tree has no children
    }
    
    public ParseTree(Variable lbl) 
    {
        label = new Knot(lbl);
        this.children = new ArrayList<ParseTree>(); // This tree has no children
    }
    
    public ParseTree(Terminal lbl) 
    {
        Symbol token = new Symbol(lbl);
        label = new Knot(token);
        this.children = new ArrayList<ParseTree>(); // This tree has no children
    }

    public Knot getKnot()
    {
        return label;
    }
    
    public List<ParseTree> getChildren()
    {
        return children;
    }
    /**
     * Creates a tree with root labeled by a Symbol lbl and children chdn.
     * 
     * @param lbl  The label of the root
     * @param chdn Its children
     */
    public ParseTree(Symbol lbl, List<ParseTree> chdn) {
        label = new Knot(lbl);
        this.children = chdn;
    }
    
     /**
     * Creates a tree with root labeled by a variable lbl and children chdn.
     * 
     * @param lbl  The label of the root as a variable
     * @param chdn Its children
     */
    
     public ParseTree(Variable lbl, List<ParseTree> chdn) {
        label = new Knot(lbl);
        this.children = chdn;
    }
    
    /**
     * Creates a tree with root labeled by a Terminal lbl and children chdn.
     * 
     * @param lbl  The label of the root as a terminal 
     * @param chdn Its children
     */
    
    public ParseTree(Terminal lbl, List<ParseTree> chdn) {
        Symbol token = new Symbol(lbl);
        Knot knot = new Knot(token);
        this.children = chdn;
    }
    
    /**
     * Writes all the nodes of the tree in a String. 
     * files.
     * @return a string with all nodes.
     */
    public String getNodes() {
        StringBuilder treeNodes = new StringBuilder();
        treeNodes.append(label.getType() + " ");
        for (ParseTree child : children) {
            treeNodes.append(child.getNodes());
        }
        return treeNodes.toString();
    }


}
