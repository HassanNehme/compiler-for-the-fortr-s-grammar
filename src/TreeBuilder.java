/**
 * A  TreeBuilder, it is used to build the parseTree 
 * in tex format; the tree can be seen as a forest picture
 * or as a TikZ picture.
 * @author Hassan Nehme
 */
public class TreeBuilder
{  
    private ParseTree tree;
    /**
    * constructor for the class TreeBuilder
    */
    public TreeBuilder(ParseTree tree)
    {
        this.tree = tree;
    }
    /**
    * Convert the string into a string in Tex format.
    * @param label The node to label by a string in Tex format
    * @return the string labelling the node
    */
   private String toTexString(Knot label)
    {   
        String texLabel = " ";
        if(label.isToken() )
        {
            if(label.getToken().isEpsilon())
                texLabel =  "$\\epsilon$";
            else if(label.getToken().getType().toString().equals(Terminal.ENDPROG.toString()))
                texLabel = "\\textbf{ENDPROG}";
            else if(label.getToken().getType().toString().equals(Terminal.ENDLINE.toString()))
            {
                texLabel = "\\textbackslash n";
            }
            else if(label.getToken().getType().toString().equals(Terminal.GT.toString()))
            {
                texLabel = "$>$";
            }
            else
                texLabel = label.getToken().getStringValue();
        }
        else if(label.isVariable())
        {
            texLabel = label.getVariable().toString();
        }
        return texLabel;
    }
    
    /**
     * Writes the tree as LaTeX code.
     * @param tree the tree to write as LaTeX code
     * @return a string containing the representation of that tree in LaTeX code
     */
    private String toLaTexTree(ParseTree tree) 
    {
        StringBuilder treeTeX = new StringBuilder();
        treeTeX.append("[");
        treeTeX.append("{" + toTexString(tree.getKnot()) + "}");
        treeTeX.append(" ");

        for (ParseTree child : tree.getChildren()) {
            treeTeX.append(toLaTexTree(child));
        }
        treeTeX.append("]");
        return treeTeX.toString();
    }
    
    /**
     * Writes the tree as TikZ code.
     * @param tree the tree to write as TikZ code
     * @return a string containing the representation of that tree in TikZ code
     */
    private String toTikZ(ParseTree tree) 
    {
        StringBuilder treeTikZ = new StringBuilder();
        treeTikZ.append("node {");
        treeTikZ.append(toTexString(tree.getKnot()));
        treeTikZ.append("}\n");
        for (ParseTree child : tree.getChildren()) {
            treeTikZ.append("child { ");
            treeTikZ.append(toTikZ(child));
            treeTikZ.append(" }\n");
        }
        return treeTikZ.toString();
    }
        
     /**
     * Writes the tree as a TikZ picture. A TikZ picture embeds TikZ code so that
     * LaTeX undertands it.
     * @return a String in LateX format that containes the tree in TikZ code with the neccessary opening and closing tags.
     */
    private String toTikZPicture() {
        return "\\begin{tikzpicture}[tree layout]\n\\" + toTikZ(tree) + ";\n\\end{tikzpicture}";
    }
    
    /**
     * Writes the tree as a forest picture. Returns the tree in forest environment
     * using the latex code of the tree
     * @return  a String in LateX format that containes the tree as a forestPicture code with the neccessary opening and closing tags.
     */
    private String toForestPicture() 
    {
        return "\\begin{forest}for tree={rectangle, draw, l sep=20pt}" + toLaTexTree(tree) + ";\n\\end{forest}";
    }
    
    /**
     * Writes the tree as a LaTeX document which can be compiled using PDFLaTeX.
     * <br>
     * <br>
     * The result can be used with the command:
     * 
     * <pre>
     * pdflatex some-file.tex
     * </pre>
     */
     public String toLaTeX() {
        return "\\documentclass[border=5pt]{standalone}\n\n\\usepackage{tikz}\n\\usepackage{forest}\n\n\\begin{document}\n\n"
                + toForestPicture() + "\n\n\\end{document}\n%% Local Variables:\n%% TeX-engine: pdflatex\n%% End:";
    }
    
      /**
     * Writes the tree as a LaTeX document which can be compiled (using the LuaLaTeX
     * engine). Be careful that such code will not compile with PDFLaTeX, since the
     * tree drawing algorithm is written in Lua. The code is not very readable as
     * such, but you can have a look at the outputted file if you want to understand
     * better. <br>
     * <br>
     * The result can be used with the command:
     * 
     * <pre>
     * lualatex some-file.tex
     * </pre>
     */
    public String toLaTeXLua() {
        return "\\RequirePackage{luatex85}\n\\documentclass{standalone}\n\n\\usepackage{tikz}\n\n\\usetikzlibrary{graphdrawing, graphdrawing.trees}\n\n\\begin{document}\n\n"
                + toTikZPicture() + "\n\n\\end{document}\n%% Local Variables:\n%% TeX-engine: luatex\n%% End:";
    }
}
