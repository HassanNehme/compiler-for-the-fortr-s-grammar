How to run the programs?

There are three options to run the code:
1.  Print on the stdout: java -jar dist/Part3.jar [INPUTFILE].fs
2.  Write in a file: java-jar dist/Part3.jar [INPUTFILE].fs -o [OUTPUTFILE].ll
3.  Interpret the file directly: java -jar dist/Part3.jar -exec [INPUTFILE].fs .

If the last option is used, the command lli out.bc can be executed to run the code. 
By default this option is already used.

Please refer to the documents in the doc directory to find all the information needed to understand
the purpose of this code.