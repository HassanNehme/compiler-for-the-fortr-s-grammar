 all:
	jflex src/LexicalAnalyzer.flex
	javac -d bin -cp src/ src/Main.java
	jar cfe dist/Part3.jar Main -C bin .	 
	javadoc -private src/Main.java src/LexicalAnalyzer.java src/LexicalUnit.java src/Symbol.java src/Parser.java src/ParseTree.java src/Knot.java src/Terminal.java src/Variable.java src/ParseException.java src/Syntax.java src/Generator.java -d doc/javadoc
testing:
	java -jar dist/Part3.jar -exec test/Factorial.fs

